#include "stdio.h"
#include <sys/types.h>
#include <sys/wait.h>
#include "stdlib.h"
#include "unistd.h"
#include <fcntl.h>           
#include <sys/stat.h>        
#include <semaphore.h>

#define NAMED_SEM "numeSem"

sem_t *sem;

void P3(){
    sem_wait(sem);
    printf("Am inceput procesul 3\n");

    printf("S-a terminat procesul 3\n");
}
void P4(){
    printf("Am inceput procesul 4\n");

    printf("S-a terminat procesul 4\n");
    sem_post(sem);
}
void P2(){
    printf("Am inceput procesul 2\n");
    int pid3 = fork();
    if(0 == pid3){
        P3();
        exit(0);
    }
    int pid4 = fork();
    if(0 == pid4){
        P4();
        exit(0);
    }
    if(pid3 != waitpid(pid3, NULL, 0)){
        printf("Nu s-a putut astepta dupa procesul 3\n");
        return;
    }
    if(pid4 != waitpid(pid4, NULL, 0)){
        printf("Nu s-a putut astepta dupa procesul 4\n");
        return;
    }
    printf("S-a terminat procesul 2\n");
}
int main(){
    printf("Am inceput procesul 1\n");
    unlink(NAMED_SEM);

    sem = sem_open(NAMED_SEM, O_CREAT, 0600, 0);

    int pid2 = fork();
    if(0 == pid2){
        P2();
        exit(0);
    }
    if(pid2 != waitpid(pid2, NULL, 0)){
        printf("Nu s-a putut astepta dupa procesul 2\n");
        return 0;
    }
    printf("S-a terminat procesul 1\n");
    return 0;
}