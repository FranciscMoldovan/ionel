#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include <semaphore.h>

int vowelCount = 0;
sem_t sema;

void* th_func(void* arg)
{
    char* myWord = arg;
    int localVowelCount = 0;
    printf("[a thread:]%s\n", myWord);
    for(int i=0; i<strlen(myWord); i++)
    {
        if (strchr("AEIOUaeiou", myWord[i]) != NULL)
        {
            localVowelCount++;
        }
    }

    sem_wait(&sema);
        printf("[thread da socoteala: {%d} vocale]\n", localVowelCount);
        vowelCount += localVowelCount;
    sem_post(&sema);

    return NULL;
}

int main(int argc, char** argv)
{
    int numThreads = argc -1;
    pthread_t* threadurile;

    if (argc == 1)
    {
        printf("USAGE: %s cuv1 cuv2 cuv3 .. \n", argv[0]);
    }
    
    for(int i=1; i<argc; i++)
    {
        printf("%s\n", argv[i]);
    }

    sem_init(&sema, 0, 1);

    threadurile = (pthread_t*)malloc(numThreads*sizeof(pthread_t));
    for(int i=0; i<numThreads; i++)
    {
        pthread_create(&threadurile[i], NULL, th_func, argv[i+1]);
    }
    for(int i=0; i<numThreads; i++)
    {
        pthread_join(threadurile[i], NULL);
    }

    printf("[MAIN TOATAL VOCALE]%d\n", vowelCount);
free(threadurile);
return 0;
}

