#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <fcntl.h>              /* Obtain O_* constant definitions */
#include <unistd.h>

#define FILE_NAME "fis.txt"

int main()
{
    int pid;
    int fd;
    int fdPipe[2];
    char aChar;

    if (-1 == pipe(fdPipe))
    {
        perror("PIPE ERROR!\n");
        return -1;
    }

    pid = fork();
    if (-1 == pid)
    {
        perror("FORK ERROR\n");
        return -1;
    }

    if (pid == 0)
    {// copil
        char aReadChar;
        printf("copil[pid=%d][ppid=%d]\n", getpid(), getppid());
        for(;;)
        {
            read(fdPipe[0], &aReadChar, 1);
            //printf("\t[CHILD read:]%c\n", aReadChar);
            if (aReadChar == 0)
            {
                break;
            }
            if ( !( (aReadChar - '0') % 2 ) )
            {
                printf("COPILUUUUL! NA PARA:%c\n", aReadChar);
            }
        }

    } else 
    {// parinte
        printf("parinte[pid=%d][ppid=%d]\n", getpid(), getppid());
        fd = open(FILE_NAME, O_RDONLY);
        if (-1 == fd )
        {
            perror("FILE OPEN ERROR\n");
            return -1;
        }
        while ( 0 != read(fd, &aChar, 1) )
        {
            printf("[parinte]%c\n", aChar);
            if (aChar >= '0' && aChar <= '9')
            {
                write(fdPipe[1], &aChar, 1);
            } 
        }
        char zeroul = 0; 
        write(fdPipe[1], &zeroul, 1);
    }

    return 0;
}